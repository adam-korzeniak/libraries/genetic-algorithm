package com.adamkorzeniak.utils.genetic_algorithm.domain;

import com.adamkorzeniak.utils.genetic_algorithm.domain.strategies.*;
import lombok.RequiredArgsConstructor;

import java.util.Set;

@RequiredArgsConstructor
public class Environment<T extends Agent> {

    private final Simulation simulation;
    private final AgentGenerator<T> agentGenerator;
    private final FitnessCalculator<T> fitnessCalculator;
    private final AgentSelector<T> agentSelector;
    private final AgentCrossover<T> agentCrossover;
    private final AgentMutator<T> agentMutator;

    private Set<T> agents;
    private Set<T> nextParents;

    public void generateInitialPopulation() {
        agents = agentGenerator.generateAgents();
    }

    public void calculateFitness() {
        for (T agent: agents) {
            double fitness = fitnessCalculator.calculateFitness(this, agent);
            agent.setFitness(fitness);
        }
        simulation.addGenerationResults();
    }

    public void chooseParents() {
        nextParents = agentSelector.selectParents(agents);
    }

    public void crossover() {
        agents = agentCrossover.crossover(nextParents);
    }

    public void mutate() {
        agents.forEach(agentMutator::mutate);
    }
}
