package com.adamkorzeniak.utils.genetic_algorithm.domain;

import lombok.Data;

@Data
public abstract class Agent {
    private double fitness;
}
