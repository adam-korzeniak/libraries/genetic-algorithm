package com.adamkorzeniak.utils.genetic_algorithm.domain.strategies;

import com.adamkorzeniak.utils.genetic_algorithm.domain.Simulation;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
public class ConvergingSimulationStrategy implements ShouldContinueStrategy {

    private final int generations;
    private final int progress;

    @Override
    public boolean test(Simulation simulation) {
        List<Double> averageFitnessHistory = simulation.getAverageFitnessHistory();
        int historySize = averageFitnessHistory.size();
        if (historySize < generations) {
            return true;
        }
        List<Double> lastGenerationsFitness = averageFitnessHistory.subList(historySize - generations, historySize);
        Double max = Collections.max(lastGenerationsFitness);
        Double min = Collections.min(lastGenerationsFitness);
        return max / min > progress;
    }
}
